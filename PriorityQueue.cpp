#include "PriorityQueue.h"
#include <exception>
using namespace std;

PriorityQueue::PriorityQueue(Relation r) {
	//TODO - Implementation
	this->head = NULL;
	this->tail = NULL;

	this->relation = r;
}
//Theta (1)


void PriorityQueue::push(TElem e, TPriority p) {
	//TODO - Implementation
    // 
    // Create a new node with the given values
    Node* node = new Node;
    node->data.first = e;
    node->data.second = p;
    node->next = NULL;

    // If head is null, then this is the first node to be added
    // so make head = the new Node
    if (head == NULL) {
        head = node;
        return;
    }

    // If we got here it means that the head is not null

    Node* temp = head;      // Create a pointer that points to the head
    Node* prev = NULL;      // Create a pointer that points behind the temp (initially NULL)

    // search for first node which corresponds to the given relation OR just the last node
    while (temp != NULL && this->relation(temp->data.second, node->data.second)) {
        prev = temp;        // The pointer that points behind gets the current point
        temp = temp->next;  // The current point goes to the next element
    }
    if (temp == NULL) {     // If we got to the last element
        // no node corresponds to the given relation (Case 1)
        prev->next = node;
    }
    else {
        if (prev == NULL) { // If we didn't go through the SLL
            // all the nodes correspond to the given relation (Case 2)
            // add this node at the starting
            node->next = head;
            head = node;
        }
        else {              // If we stopped somewhere in between the head and the tail
            // Case 3
            // add this node before the node which corresponds to the given relation
            node->next = temp;
            prev->next = node;
        }
    }
}
//Theta (n)

//throws exception if the queue is empty
Element PriorityQueue::top() const {
	//TODO - Implementation
    // 
    // head of the linked list contains the maximum priority element
    if (this->head != NULL) {
        return head->data;
    }
    throw exception();
}
//Theta (1)

Element PriorityQueue::pop() {
	//TODO - Implementation

    // head of the linked list contains the maximum priority element
    if (this->head != NULL) {
        Element data = this->head->data;

        // removing the highest priority element while saving its data in order to return it
        Node* remove = this->head;
        this->head = this->head->next;
        delete remove;

        return data;
    }
    throw exception();
}
//Theta (1)

bool PriorityQueue::isEmpty() const {
	//TODO - Implementation

	if (this->head == NULL) {
		return true;
	}
	else {
		return false;
	}
}
//Theta (1)

PriorityQueue::~PriorityQueue() {
	//TODO - Implementation
    Node* current = this->head;
    Node* next = NULL;
    while (current != NULL) {
        next = current->next;
        delete current;
        current = next;
    }
    this->head = NULL;
    this->tail = NULL;
};
//Theta (n)
